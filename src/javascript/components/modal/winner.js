import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const winnerInfo = {
    title: 'And WBA goes to!',
    bodyElement: fighter.name
  }
  
  showModal(winnerInfo);
}
